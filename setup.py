import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="flask-docker",  # Replace with your own username
    version="0.0.0",
    author="Me",
    author_email="me@example.com",
    description="Example of using flask and deploy",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/KRGKOKO/flask-docker",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: None",
        "Operating System :: OS Independent",
    ],
    python_requires=">=3.8",
)
