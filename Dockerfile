FROM python:3.8-alpine as builder

RUN mkdir /app
COPY . /app
WORKDIR /app

RUN apk add --update \
    gcc \
    musl-dev \
    linux-headers

RUN pip install wheel==0.34.2 && pip wheel . --wheel-dir=/app/wheels --no-binary :all:

FROM python:3.8-alpine as production

COPY --from=builder /app /app
WORKDIR /app

RUN pip install -r requirements.txt
RUN pip install \
    --no-index \
    --find-links=/app/wheels \
    /app \
    --no-binary :all:

EXPOSE 5000

ENTRYPOINT [ "python3" ]
CMD [ "server.py" ]