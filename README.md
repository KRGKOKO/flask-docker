# Flask Docker

This project for person who want to build lite image on Flask server.

Maybe there is better solution from this

## Install requirements

```bash
pip3 install -r requirements.txt
```

## Test build dist

Follow the `setup.py` template

```bash
python3 setup.py sdist bdist_wheel
```

## Test docker build

```bash
docker-compose up -d --force-recreate --build
```
